import ChildAgeInputs from 'src/pages/ChildAgeInputs';
import {expect} from 'chai';

describe('User is able to specify age of each child', ()=>{
  it('works', async ()=>{
    const numberOfChilds: number = 4;

    await ChildAgeInputs.open;

    //open drop down for select number of peoples
    ChildAgeInputs.openDropDown();

    //add N child number
    await ChildAgeInputs.addChildSeats(numberOfChilds);

    //checking for age inputs number
    await expect(await ChildAgeInputs.getChildsAgeSelectorsNumber()).to.equal(numberOfChilds);
  });
});