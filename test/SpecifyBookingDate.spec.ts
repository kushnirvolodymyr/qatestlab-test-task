import SpecifyBookingDate from 'src/pages/SpecifyBookingDate';
import {expect} from 'chai';

describe("User is required to specify booking date to see booking price", ()=>{
  it('city search', async ()=>{
    browser.deleteAllCookies();
    //open browser
    await SpecifyBookingDate.open;

    //city name
    const city = 'Lviv';

    //search city by name
    await SpecifyBookingDate.searchCity(city);

    //deriveved date selector
    //const picker = await SpecifyBookingDate.datePicker;

    //url check
    const onPage = (await browser.getUrl()).indexOf('searchresults') !== -1;

    //is prices is visible
    const pricesIsVisibles = await SpecifyBookingDate.isPricesVisible();

    //wait until date selector has been shown
    /*await browser.waitUntil(()=>{
      return picker.isDisplayed();
    }, 5000, 'picker not displayed');
    */
    //check if picker is shown
    //const pickerIsOpen = await picker.isDisplayed();
    
    await expect(onPage && await SpecifyBookingDate.datepickerIsOpen() && !pricesIsVisibles).to.equals(true);
    //check url
    //expect(await browser.getUrl()).to.include('searchresults');
  });

  it('show prices', async ()=>{
    //get first button on page
    const showPriceButton = await SpecifyBookingDate.priceButtons[0];
    //click shwo price button
    showPriceButton.click();
    //check if datepicker is open
    expect(await SpecifyBookingDate.datepickerIsOpen()).to.equal(true);
  })
  
  it('select date', async ()=>{
    //get datesearch button element
    const dateSearch = await SpecifyBookingDate.dateSearchButton;
    //get datepicker element
    const datePicker = await SpecifyBookingDate.datePicker;
    //select day in datepicker
    await SpecifyBookingDate.selectDay(5);
    //wait until datepicker will close, in other way cant click on search button
    await browser.waitUntil(()=>{
      return !datePicker.isDisplayed()
    }, 5000, 'picker dont closed');
    //click search button
    await dateSearch.click();
    //check for expects
    expect(await SpecifyBookingDate.allSrItemsHavePriceOrBanner()).to.equal(true);
  })

});
