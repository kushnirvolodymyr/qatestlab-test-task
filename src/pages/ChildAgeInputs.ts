import BasePage from './BasePage'

class ChildAgeInputs extends BasePage{

  get seatsSelectDropdown(): WebdriverIO.Element{
    return $('#xp__guests__toggle');
  }

  get addChildSeatButton():WebdriverIO.Element{
    return $('.sb-group-children .bui-stepper__add-button');
  }

  get childAgeSelectors(): WebdriverIO.Element[]{
    return $$('.sb-group__children__field select[name="age"]');
  }
  
  /**
   * Open seats number selector
   *
   * @memberof ChildAgeInputs
   */
  async openDropDown(){
    const dd = await this.seatsSelectDropdown;
    await dd.click();
  }

  /**
   * Add N number to the childs seats
   *
   * @param {number} seatsNumber Number od child seats
   * @memberof ChildAgeInputs
   */
  async addChildSeats(seatsNumber: number){
    const acb:WebdriverIO.Element = await this.addChildSeatButton;

    // click n times on "+" childs seats button
    for(let i = 0; i < seatsNumber; i++){
      await acb.click();
    }
  }

  /**
   * Get number of childs age selectors
   *
   * @returns {Promise<number>}
   * @memberof ChildAgeInputs
   */
  async getChildsAgeSelectorsNumber(): Promise<number>{
    //get array of selectors
    const selectors: WebdriverIO.Element[] = await this.childAgeSelectors;

    //return array length
    return selectors.length
  }
}

export default new ChildAgeInputs();