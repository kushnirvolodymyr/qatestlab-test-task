import BasePage from "./BasePage";

class SpecifyBookingDate extends BasePage{


  /**
   * Destinaton city search form
   *
   * @readonly
   * @type {WebdriverIO.Element}
   * @memberof SpecifyBookingDate
   */
  get destinationCitySearchInput():WebdriverIO.Element{
    return $('.sb-destination__input');
  }


  /**
   * Get list with city autocomplete results
   *
   * @readonly
   * @type {WebdriverIO.Element[]}
   * @memberof SpecifyBookingDate
   */
  get citySearchAutocomplete():WebdriverIO.Element[]{
    return $$('.sb-destination .sb-autocomplete__list.-visible .sb-autocomplete__item');
  }

  /**
   * City search button
   *
   * @readonly
   * @type {WebdriverIO.Element}
   * @memberof SpecifyBookingDate
   */
  get searchButton():WebdriverIO.Element{
    return $('.sb-searchbox__button');
  }

  get datePicker(): WebdriverIO.Element{
    return $('.sb-dates .sb-dates__col.--checkin-field .c2-calendar');
  }

  get datePickerDays(): WebdriverIO.Element[]{
    return $$('.sb-dates .sb-dates__col.--checkin-field .c2-calendar td.c2-day')
  }

  get priceButtons(): WebdriverIO.Element[]{
    return $$('.sr_cta_button.no_dates_click');
  }

  get dateSearchButton(): WebdriverIO.Element{
    return $('.sb-searchbox__button');
  }

  get searchResuts(): WebdriverIO.Element[]{
    return $$('#hotellist_inner .sr_item')
  }

  get roomSoldOut(): WebdriverIO.Element[]{
    return $$('.sold_out_property_wrapper');
  }

  get roomPrices(): WebdriverIO.Element[]{
    return $$('.roomPrice');
  }
  /**
   *  Select city in search field
   *
   * @param {string} city
   * @memberof SpecifyBookingDate
   */
  async searchCity(city: string){
    const dcsi = await this.destinationCitySearchInput;
    const sb = await this.searchButton;
    
    //put city name in search input
    await dcsi.addValue(city);
    
    //wait until loads search autocomplete results
    await browser.waitUntil(()=>{
      return this.citySearchAutocomplete.length > 0;
    }, 10000, 'autocomlete is empty');
    
    //load autocomplete variants
    const csac = await this.citySearchAutocomplete;
    
    //select first element from autocomplete
    await browser.waitUntil(()=>{
      return csac[0].isDisplayed();
    }, 5000, 'cant focus');
    await csac[0].click();


    //click on searh button
    await sb.click();
  }

  async isPricesVisible():Promise<boolean>{
    //if buttons available on page
    const btns = await this.priceButtons;
    //then prices not shown
    return !(btns.length > 0);
  }

  /**
   *  Checking if datepicker is open
   *
   * @returns {Promise<boolean>}
   * @memberof SpecifyBookingDate
   */
  async datepickerIsOpen(): Promise<boolean>{
    //get datepicker element
    const picker = await this.datePicker;
    //wait until piker will be displayed
    await browser.waitUntil(()=>{
      return picker.isDisplayed();
    }, 5000, 'picker not displayed');
    //checking
    const pickerIsOpen = await picker.isDisplayed();

    return pickerIsOpen;
  }

  /**
   * Select day in datepicker
   *
   * @param {number} dayNumber
   * @memberof SpecifyBookingDate
   */
  async selectDay(dayNumber: number){
    const days = await this.datePickerDays;

    days[dayNumber].click();
  }


  /**
   * Checking for "Expect: each result entry has booking price or banner saying no free places"
   *
   * @returns {Promise<boolean>}
   * @memberof SpecifyBookingDate
   */
  async allSrItemsHavePriceOrBanner():Promise<boolean>{
    //list of search items
    const items = await this.searchResuts;
    //list of prices
    const prices = await this.roomPrices;
    //list of solded aartaments
    const sold = await this.roomSoldOut;
    //if sum of items width prices and solded equall all itemsM then test passed
    return items.length === (prices.length + sold.length);
  }
}

export default new SpecifyBookingDate();