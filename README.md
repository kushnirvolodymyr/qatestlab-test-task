Тестовое задание на вакансию тестировщика програмного обеспечения в QATestLab.
Исполнитель Кушнир Владимир.
Перед запуском необходимо выполнить команду "npm install".
После того как необходимые пакеты установятся, для запуска тестов необходимо ввести комманду "npm  test".

Test job for vacancy "Software Tester" in QATestLab.
Author - Kushnir Vladimir.
Before starting, you must run the command "npm install".
After the necessary packages are installed, to run the tests, you must enter the command "npm test".